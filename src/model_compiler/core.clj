(ns model-compiler.core
  (:require [model-compiler.parse :as parse]
            [model-compiler.mps-file :as mps]
            [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [schema.core :as s])
  (:gen-class))

(def restriction "11.1x -  2.8 y \\leq 3")

(def objective "1.0x - 1 + y")

(s/defschema Model
  {:model s/Str})

(def app
  (api
    {:swagger
     {:ui "/"
      :spec "/swagger.json"
      :data {:info {:title "Model compiler"
                    :description "An API to convert latex mip models to mps"}
             :tags [{:name "api", :description "To MPS"}]}}}

    (context "/api" []
      :tags ["api"]
      
      (POST "/to-mps" []
        :body [json Model]
        :summary "Latex to MPS"
        (ok {:return (mps/build-final-mps (:model json))})))))
