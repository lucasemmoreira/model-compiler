(ns model-compiler.parse
  (:require [instaparse.core :as insta :refer [defparser]]
            [clojure.walk :as walk])
  (:gen-class))

(def restriction "11.1x -  2.8 y \\leq 3")

(def objective "1.0x + y")

(def multiple-restriction
  "11.1x - 2.8 y \\leq 3 \\\\
  22.1x - 4.8 y \\leq 18 \\\\")

(def atom-name (atom 0))

(defn break-constraint [constraints])

(defparser rules (clojure.java.io/resource "parser.bnf"))

(defn strip
  "Simple function to remove all whitespaces from string before ruling it"
  [string]
  (clojure.string/replace (clojure.string/replace string " " "") "\n" ""))

(defn parse [constraint]
  (rules (strip constraint)))

(defn to-float [num]
  (case num
    "+" 1
    "-" -1
    (Float. num)))

(defn cases [vector]
  (case (first vector)
    :var {:var (second vector)}
    :number {:number (to-float (second vector))}
    :term (apply merge (rest vector))
    :expression {:lhs (rest vector)}
    :rhs {:rhs (rest vector)}
    :relation-operator {:relation-operator (second vector)}
    :constraint (apply merge (rest vector))
    :linebreak vector
    vector))

(defn transform [object]
  (walk/postwalk (fn [v] (if (vector? v) (cases v) v))
                 object))

(defn mparse [constraint]
  (transform (parse constraint)))

(defn name-constraint [constraint]
  (if (contains? constraint :name)
    constraint
    (do (swap! atom-name inc)
        (assoc constraint :name (str "C" @atom-name)))))

(defn column [{:keys [lhs rhs name]}]
  (let [var-lhs (filter #(contains? % :var) lhs)
        var-rhs (map #(update % :number -) (filter #(contains? % :var) rhs))
        with-name (fn [term] (assoc term :name name))]
    (concat (map with-name var-rhs) (map with-name var-lhs))))

(column (name-constraint (mparse restriction)))

(defn rhs [{:keys [lhs rhs name]}]
  (let [num-lhs (map #(update % :number -) (filter #(not (contains? % :var)) lhs))
        num-rhs (filter #(not (contains? % :var)) rhs)]
    {:number (apply + (concat (map :number num-lhs) (map :number num-rhs)))
     :name name}))

(rhs (name-constraint (mparse restriction)))

(defn rows [constraint]
  (dissoc constraint :lhs :rhs))

(rows (name-constraint (mparse restriction)))
