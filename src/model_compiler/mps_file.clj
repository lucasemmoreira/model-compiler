(ns model-compiler.mps-file
  (:require [model-compiler.parse :as parse]))

(def restriction "11.1x -  2.8 y \\leq 3")

(def objective "1.0x - 1 + y")

(def multiple-restriction
  "11.1x -  2.8 y \\leq 3 \\\\
  22.1x -  4.8 y \\leq 18 \\\\")

(defn str-col [string column new-input]
  (let [so-far (count string)
        n-chars (count (str new-input))
        space-2-skip (dec (+ n-chars (- column so-far)))]
    (str string (format (str "%" space-2-skip "s") (str new-input)))))

(str-col "" 2 "L")

(defn row [constraint]
  (let [{:keys [relation-operator name]} constraint
        letter (case relation-operator
                 "\\leq" "L"
                 "\\geq" "G"
                 "\\eq" "E"
                 "N")]
    (str-col (str-col "" 2 letter) 5 name)))

(row restriction)

(row objective)

(defn column [constraint]
  (let [terms (parse/column constraint)]
    (loop [[{:keys [number var name]} & rest-term] terms
           string []]
      (if (some nil? [number var name])
        string
        (let [term-mps (str-col (str-col (str-col "" 5 var) 15 name) 25 number)]
          (recur rest-term (conj string term-mps)))))))

(column restriction)

(defn rhs [constraint]
  (let [{:keys [name number]} (parse/rhs constraint)]
    (str-col (str-col "     B" 15 name) 25 number)))

(rhs restriction)

(defn name-constraint [constraint]
  (parse/name-constraint (parse/mparse constraint)))

(defn build-final-mps [model]
  (let [constraints (map name-constraint (clojure.string/split model #"\\\\"))
        rows (map row constraints)
        columns (apply concat (map column constraints))
        rhss (map rhs constraints)
        end-line (fn [line] (str line " \n"))
        content (str (apply str "ROWS \n" (map end-line rows))
                     (apply str "COLUMNS \n" (map end-line columns))
                     (apply str "RHS \n" (map end-line rhss)))]
    (spit "model.mps" content)
    "saved in 'model.mps'. Search for it in your root project"))

(build-final-mps multiple-restriction)
