#+TITLE: MIP Model Compiler
#+AUTHOR: Lucas Moreira
#+EMAIL: lucasemmoreira@gmail.com

* What is?
  The purpose of this project is to have an easy way to export a latex mip model to MPS file.


* Compiler for tex to solver format
  The project is still in alpha. For now, it only reads simple equations (no \sum, no \forall and no bounds) and it is being worked to export to MPS. That being said, an example of working constraint is 

#+BEGIN_SRC latex
2x - 2y \leq 2
#+END_SRC

* How to play with it?
  As a clojure project, you need to have [[https://clojure.org/guides/getting_started][Clojure]] and [[https://leiningen.org/][Leiningen]] installed. If you would to taste it, you can go to the root project and run

#+BEGIN_SRC bash
lein ring server
#+END_SRC

It will open an swagger website in your browser. There you can put a constraint example:

#+BEGIN_SRC JSON
{"model": "1.1x + 2.3y \\leq 3 \\\\ 2.2y \\geq 5"}
#+END_SRC

* The API
  For now it only works with http requests. The idea is to send a string with the model to the url

#+BEGIN_SRC bash
http://localhost:port/api/to-mps
#+END_SRC

If successful, it will save a file 'model.mps'.

