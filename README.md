
# Table of Contents

1.  [What is?](#orgabd53f3)
2.  [Compiler for tex to solver format](#org159e27f)
3.  [How to play with it?](#org5d604ac)
4.  [The API](#org063c294)



<a id="orgabd53f3"></a>

# What is?

The purpose of this project is to have an easy way to export a latex mip model to MPS file.


<a id="org159e27f"></a>

# Compiler for tex to solver format

The project is still in alpha. For now, it only reads simple equations (no &sum;, no &forall; and no bounds) and it is being worked to export to MPS. That being said, an example of working constraint is 

    2x - 2y \leq 2


<a id="org5d604ac"></a>

# How to play with it?

As a clojure project, you need to have [Clojure](https://clojure.org/guides/getting_started) and [Leiningen](https://leiningen.org/) installed. If you would to taste it, you can go to the root project and run

    lein ring server

It will open an swagger website in your browser. There you can put a constraint example:

    {"model": "1.1x + 2.3y \\leq 3 \\\\ 2.2y \\geq 5"}


<a id="org063c294"></a>

# The API

For now it only works with http requests. The idea is to send a string with the model to the url

    http://localhost:port/api/to-mps

If successful, it will save a file 'model.mps'.

